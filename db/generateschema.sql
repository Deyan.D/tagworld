create or replace schema job_task;
create or replace table job_task.users
(
    id       bigint auto_increment
        primary key,
    username varchar(30) not null,
    password varchar(25) not null,
    email    varchar(40) not null,
    constraint users_pk
        unique (email),
    constraint users_pk2
        unique (username)
);

create or replace table job_task.tags
(
    id          bigint auto_increment
        primary key,
    author_id   bigint       not null,
    content     longtext     not null,
    description varchar(100) not null,
    title       varchar(50)  not null,
    constraint tags_pk
        unique (title),
    constraint tags_users_fk
        foreign key (author_id) references users (id)
);


