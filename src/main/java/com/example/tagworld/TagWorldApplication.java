package com.example.tagworld;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TagWorldApplication {

    public static void main(String[] args) {
        SpringApplication.run(TagWorldApplication.class, args);
    }

}
