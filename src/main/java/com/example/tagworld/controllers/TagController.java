package com.example.tagworld.controllers;

import com.example.tagworld.exceptions.DuplicateEntityException;
import com.example.tagworld.exceptions.EntityNotFoundException;
import com.example.tagworld.exceptions.UnauthorizedOperationException;
import com.example.tagworld.helpers.AuthenticationHelper;
import com.example.tagworld.models.Tag;
import com.example.tagworld.models.User;
import com.example.tagworld.models.dtos.TagDtoIn;
import com.example.tagworld.services.TagService;
import com.example.tagworld.services.mappers.TagMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/tags")
public class TagController {
    private final TagService tagService;
    private final TagMapper tagMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public TagController(TagService tagService, TagMapper tagMapper, AuthenticationHelper authenticationHelper) {
        this.tagService = tagService;
        this.tagMapper = tagMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Tag> getAll() {
        return tagService.getAll();
    }

    @GetMapping("/{id}")
    public Tag getById(@PathVariable long id) {
        try {
            return tagService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
    @PostMapping
    public Tag create(@RequestHeader HttpHeaders headers, @Valid @RequestBody TagDtoIn dtoIn) {

        try {
            User user = authenticationHelper.tryGetLogged(headers);
            Tag tag = tagMapper.dtoToObject(dtoIn);
            tag.setAuthor(user);
            return tagService.create(tag);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Tag update(@RequestHeader HttpHeaders headers, @PathVariable long id, @Valid @RequestBody TagDtoIn dtoIn) {
        try {
            User user = authenticationHelper.tryGetLogged(headers);
            Tag tag = tagMapper.dtoToObject(dtoIn);
            return tagService.update(user, tag, id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable long id) {
        try {
            User user = authenticationHelper.tryGetLogged(headers);
            tagService.delete(user, id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
