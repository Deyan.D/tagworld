package com.example.tagworld.enums;

public enum UserRole {
    REGULAR, MODERATOR, ADMIN
}
