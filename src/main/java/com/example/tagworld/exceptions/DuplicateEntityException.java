package com.example.tagworld.exceptions;

public class DuplicateEntityException extends RuntimeException {

    public DuplicateEntityException(String type, String attribute) {
        super(String.format("%s with the same %s already exists.", type, attribute));
    }

    public DuplicateEntityException(String type, String attribute, String typeAtt) {
        super(String.format("%s with the same %s and %s already exists", type, attribute, typeAtt));
    }
}
