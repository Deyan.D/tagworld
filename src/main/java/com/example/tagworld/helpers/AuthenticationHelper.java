package com.example.tagworld.helpers;

import com.example.tagworld.exceptions.EntityNotFoundException;
import com.example.tagworld.models.User;
import com.example.tagworld.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;


@Component
public class AuthenticationHelper {

    public static final String AUTHORIZATION_HEADER_NAME = "Authorization";
    public static final String INVALID_AUTHENTICATION_ERROR = "Invalid authentication";
    public static final String REQUIRES_AUTHENTICATION = "The requested resource requires authentication";

    private final UserService userService;

    @Autowired
    public AuthenticationHelper(UserService userService) {
        this.userService = userService;
    }


    private void checkForAuthorizationHeader(HttpHeaders headers) {
        if (!headers.containsKey(AUTHORIZATION_HEADER_NAME)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,
                    REQUIRES_AUTHENTICATION);
        }
    }

    public User tryGetLogged(HttpHeaders headers) {
        checkForAuthorizationHeader(headers);
        try {
            String accountCredentials = headers.getFirst(HttpHeaders.AUTHORIZATION);

            String username = getUsername(accountCredentials);
            String password = getPassword(accountCredentials);

            User user = userService.getByUsername(username);

            if (!user.getPassword().equals(password)) {
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, INVALID_AUTHENTICATION_ERROR);
            }
            return user;

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,
                    INVALID_AUTHENTICATION_ERROR);
        }
    }


    private String getPassword(String userInfo) {
        int index = userInfo.indexOf(" ");
        if (index == -1) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, INVALID_AUTHENTICATION_ERROR);
        }
        return userInfo.substring(index + 1);
    }

    private String getUsername(String userInfo) {
        int index = userInfo.indexOf(" ");
        if (index == -1) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, INVALID_AUTHENTICATION_ERROR);
        }
        return userInfo.substring(0, index);
    }

}
