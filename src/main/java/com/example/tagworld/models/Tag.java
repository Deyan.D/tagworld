package com.example.tagworld.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name="tags")
@Getter
@Setter
@RequiredArgsConstructor
public class Tag {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private long id;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "author_id", nullable = false)
    private User author;

    @Column(name="content")
    private String content;

    @Column(name="description")
    private String description;

    @Column(name="title")
    private String title;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Tag tag)) return false;
        return id == tag.id && author.equals(tag.author) && content.equals(tag.content) && description.equals(tag.description) && title.equals(tag.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, author, content, description, title);
    }
}
