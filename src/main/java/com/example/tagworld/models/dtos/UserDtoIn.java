package com.example.tagworld.models.dtos;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
public class UserDtoIn {

    @NotNull
    @Size(min=5, max = 30)
    private String username;

    @NotNull
    @Size(min=5, max = 25)
    private String password;

    @NotNull
    @Size(max = 40)
    @Email(message = "Email should have valid email address structure")
    private String email;
}
