package com.example.tagworld.repositories;

import com.example.tagworld.models.Tag;
import org.springframework.data.jpa.repository.JpaRepository;


public interface TagRepository extends JpaRepository<Tag, Long> {
    boolean existsTagByTitleEqualsIgnoreCase(String title);
}
