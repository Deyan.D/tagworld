package com.example.tagworld.repositories;


import com.example.tagworld.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    boolean existsUserByUsernameEqualsIgnoreCase(String username);
    boolean existsUserByEmailEqualsIgnoreCase(String email);

    Optional<User> findUserByUsernameEqualsIgnoreCase(String username);
}
