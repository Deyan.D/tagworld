package com.example.tagworld.services;

import com.example.tagworld.models.Tag;
import com.example.tagworld.models.User;

import java.util.List;

public interface TagService {
    List<Tag> getAll();
    Tag getById(long id);
    Tag create(Tag tag);
    Tag update(User user, Tag tag, long id);
    void delete(User user, long id);
}
