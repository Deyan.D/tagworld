package com.example.tagworld.services;

import com.example.tagworld.exceptions.DuplicateEntityException;
import com.example.tagworld.exceptions.EntityNotFoundException;
import com.example.tagworld.exceptions.UnauthorizedOperationException;
import com.example.tagworld.models.Tag;
import com.example.tagworld.models.User;
import com.example.tagworld.repositories.TagRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TagServiceImpl implements TagService {

    public static final String UNAUTHORIZED_TAG_OPERATION = "Only tag authors can modify or delete tags.";
    private final TagRepository tagRepository;

    public TagServiceImpl(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    @Override
    public List<Tag> getAll() {
        return tagRepository.findAll();
    }

    @Override
    public Tag getById(long id) {
        Optional<Tag> result = tagRepository.findById(id);
        return result.orElseThrow(()-> new EntityNotFoundException("Tag", id));
    }

    @Override
    public Tag create(Tag tag) {
        if(tagRepository.existsTagByTitleEqualsIgnoreCase(tag.getTitle())) {
            throw new DuplicateEntityException("Tag", "Title");
        }
        return tagRepository.save(tag);

    }

    @Override
    public Tag update(User user, Tag tag, long id) {
        Tag tagToUpdate = getById(id);

        if(!user.equals(tagToUpdate.getAuthor())) {
            throw new UnauthorizedOperationException(UNAUTHORIZED_TAG_OPERATION);
        }

        if(tagRepository.existsTagByTitleEqualsIgnoreCase(tag.getTitle()) &&
                !tag.getTitle().equals(tagToUpdate.getTitle())) {
            throw new DuplicateEntityException("Tag", "Title");
        }

        tag.setId(id);
        tag.setAuthor(tagToUpdate.getAuthor());
        return tagRepository.save(tag);
    }

    @Override
    public void delete(User user, long id) {
        Tag tagToDelete = getById(id);
        if(!user.equals(tagToDelete.getAuthor())) {
            throw new UnauthorizedOperationException(UNAUTHORIZED_TAG_OPERATION);
        }
        tagRepository.delete(tagToDelete);
    }
}
