package com.example.tagworld.services;

import com.example.tagworld.models.User;

import java.util.List;

public interface UserService {

    List<User> getAll();
    User getById(long id);
    User getByUsername(String username);
    User create(User user);
    User update(User editor, User user, long id);
    void delete(User editor, long id);
}
