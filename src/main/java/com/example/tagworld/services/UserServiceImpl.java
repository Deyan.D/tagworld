package com.example.tagworld.services;

import com.example.tagworld.exceptions.DuplicateEntityException;
import com.example.tagworld.exceptions.EntityNotFoundException;
import com.example.tagworld.exceptions.UnauthorizedOperationException;
import com.example.tagworld.models.User;
import com.example.tagworld.repositories.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    public static final String USER_MODIFY_AUTHORIZATION_ERROR = "Users can only modify their own profiles!";
    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }

    @Override
    public User getById(long id) {
        Optional<User> result = userRepository.findById(id);
        return result.orElseThrow(()-> new EntityNotFoundException("User", id));
    }

    @Override
    public User getByUsername(String username) {
        Optional<User> result = userRepository.findUserByUsernameEqualsIgnoreCase(username);
        return result.orElseThrow(()-> new EntityNotFoundException("User", "username", username));
    }

    @Override
    public User create(User user) {
        if(userRepository.existsUserByUsernameEqualsIgnoreCase(user.getUsername())) {
            throw new DuplicateEntityException("User", "username");
        }
        if(userRepository.existsUserByEmailEqualsIgnoreCase(user.getEmail())) {
            throw new DuplicateEntityException("User", "email");
        }
        return userRepository.save(user);
    }

    @Override
    public User update(User editor, User user, long id) {
        User userToUpdate = getById(id);
        if(!editor.equals(userToUpdate)) {
            throw new UnauthorizedOperationException(USER_MODIFY_AUTHORIZATION_ERROR);
        }
        if(userRepository.existsUserByUsernameEqualsIgnoreCase(user.getUsername())
                && !user.getUsername().equals(userToUpdate.getUsername())) {
            throw new DuplicateEntityException("User", "username");
        }
        if(userRepository.existsUserByEmailEqualsIgnoreCase(user.getEmail())
                && !user.getEmail().equals(userToUpdate.getEmail())) {
            throw new DuplicateEntityException("User", "email");
        }

        user.setId(id);
        user.setAuthoredTags(userToUpdate.getAuthoredTags());
        return userRepository.save(user);
    }

    @Override
    public void delete(User editor, long id) {
        User userToDelete = getById(id);
        if(!editor.equals(userToDelete)) {
            throw new UnauthorizedOperationException(USER_MODIFY_AUTHORIZATION_ERROR);
        }
        userRepository.delete(userToDelete);
    }
}
