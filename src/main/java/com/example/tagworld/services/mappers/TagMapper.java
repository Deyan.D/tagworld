package com.example.tagworld.services.mappers;

import com.example.tagworld.models.Tag;
import com.example.tagworld.models.dtos.TagDtoIn;
import org.springframework.stereotype.Component;

@Component
public class TagMapper {
    public Tag dtoToObject(TagDtoIn dtoIn) {
        Tag result = new Tag();
        result.setTitle(dtoIn.getTitle());
        result.setContent(dtoIn.getContent());
        result.setDescription(dtoIn.getDescription());
        return result;
    }
}
