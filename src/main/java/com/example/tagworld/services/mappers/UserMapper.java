package com.example.tagworld.services.mappers;

import com.example.tagworld.models.User;
import com.example.tagworld.models.dtos.UserDtoIn;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {
    public User dtoToObject(UserDtoIn dtoIn) {
        User user = new User();
        user.setUsername(dtoIn.getUsername());
        user.setPassword(dtoIn.getPassword());
        user.setEmail(dtoIn.getEmail());

        return user;
    }
}
